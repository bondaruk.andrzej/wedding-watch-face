# Wedding Watch Face

It's a simple fun project that implements a WearOS watch face with an integrated timer that is counting to a selected date. I decided that it might be a good demo of the coding style and practices that I do follow alongside some technologies that are used here.

Exemplary rendering below.

![](readme/res/watch_face_rendering.jpg)

## Description

I wrote this project before my wedding day (one would say that there is no free time before the wedding but don't worry I started soon enough). My motivation was to have a nice wedding accent for my watch that is themed exactly the same as wedding invitations which includes:
* the same text fonts
* consistent layout coloring
* background that is taken from the back of the invitation

**Disclaimer:** Obviously I'm aware that there are people that would say that doing something like that is "cringe" but still I had a lot of fun :)

## Project details

### Technologies used

* Android
* Kotlin
* RxJava
* Koin (DI)
* Timber

### CI/CD

The project has a continuous integration pipeline configured. It's running two stages:
* Build (has two jobs)
    * assembleDebug - creates an apk artifact in debug version
    * lintDebug - quality assurance
* Test
    * debugTests - it's running unit tests that are integrated into the project, the result is displayed as a report inside the executed pipeline

### Manual installation

Even though the recommended way to get the application artifact is to use the CI pipeline still it's possible to build the application manually using gradle commands.

> The application was never meant to be released so there is no release KeyStore generated and it's only possible to build debug artifacts.

To build an apk file the following command must be used:

    ./gradlew assembleDebug
To run linter the following command must be used:

    ./gradlew lintDebug
To run unit tests the following command must be used:

    ./gradlew testDebug

### Android Studio

Naturally, it's possible to run the project directly from the AS IDE itself. The minimum requirements that must be met are the following:
* Android Studio 2021.2.1 (or newer)
* Java 11 (or newer)
