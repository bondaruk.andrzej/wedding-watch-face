package pl.andrzej.weddingwatchface

import com.google.common.truth.Truth.assertThat
import io.mockk.every
import io.mockk.mockkStatic
import io.mockk.unmockkStatic
import org.junit.After
import org.junit.Before
import org.junit.Test
import pl.andrzej.weddingwatchface.provider.date.DateProvider
import pl.andrzej.weddingwatchface.provider.date.DateProvider.Companion.TIME_LEFT_WEDDING_FORMATTER
import pl.andrzej.weddingwatchface.provider.date.TimeLeft
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class DateProviderTest {

    private val dateProvider = DateProvider()
    private val testDateFormat = DateTimeFormatter.ofPattern(TIME_LEFT_WEDDING_FORMATTER, Locale.getDefault())

    @Before
    fun setUp() {
        mockkStatic(LocalDateTime::class)
    }

    @After
    fun tearDown() {
        unmockkStatic(LocalDateTime::class)
    }

    @Test
    fun getTimeAsText() {
        val testCurrentDate = LocalDateTime.parse("02/04/2021 05:10:15", testDateFormat)
        every { LocalDateTime.now() } returns testCurrentDate

        val result = dateProvider.getTimeAsText()

        val expected = "05:10"
        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun getTimeLeft() {
        val testCurrentDate = LocalDateTime.parse("02/04/2021 05:10:15", testDateFormat)
        every { LocalDateTime.now() } returns testCurrentDate

        val targetDateText = "30/07/2021 17:00:00"
        val result = dateProvider.getTimeLeft(targetDateText)

        val expected = TimeLeft(119, 11, 49, 45)
        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun getTimeLeftWithTimeShift() {
        val testCurrentDate = LocalDateTime.parse("30/01/2021 16:00:00", testDateFormat)
        every { LocalDateTime.now() } returns testCurrentDate

        val targetDateText = "30/07/2021 17:00:00"
        val result = dateProvider.getTimeLeft(targetDateText)

        val expected = TimeLeft(181, 1, 0, 0)
        assertThat(result).isEqualTo(expected)
    }

    @Test
    fun getTimeLeftDisplayFormat() {
        val testCurrentDate = LocalDateTime.parse("02/04/2021 05:10:15", testDateFormat)
        every { LocalDateTime.now() } returns testCurrentDate

        val targetDateText = "30/07/2021 17:00:00"
        val result = dateProvider.getTimeLeft(targetDateText).getTimeLeftDisplayFormat()

        // time difference 119 days, 11 hours, 49 minutes and 45 seconds
        val expected = "119d 11h 50m"
        assertThat(result).isEqualTo(expected)
    }
}
