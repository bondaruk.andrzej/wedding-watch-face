package pl.andrzej.weddingwatchface.drawer

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Rect
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import pl.andrzej.weddingwatchface.R
import pl.andrzej.weddingwatchface.ext.getFontHeight
import pl.andrzej.weddingwatchface.provider.PaintProvider
import pl.andrzej.weddingwatchface.provider.date.DateProvider

class WatchFaceDrawerImpl(private val context: Context) : WatchFaceDrawer, KoinComponent {

    private val dateProvider: DateProvider by inject()
    private val paintProvider: PaintProvider by inject()

    private var centerX = 0f
    private var centerY = 0f
    private var timeYOffset = 0f
    private var countYOffset = 0f
    private var logoYOffset = 0f
    private lateinit var watchBackground: Bitmap

    override fun onCreate() {
        watchBackground = BitmapFactory.decodeResource(context.resources, R.drawable.background)
    }

    override fun onSurfaceChanged(width: Int, height: Int) {
        centerX = width / 2f
        centerY = height / 2f
        timeYOffset = centerY + (paintProvider.timePaint.getFontHeight() / 2.0f) - 10f
        countYOffset = timeYOffset + paintProvider.counterPaint.getFontHeight() + 10f
        logoYOffset = paintProvider.logoPaint.getFontHeight() + 40f

        val scale = width.toFloat() / watchBackground.width.toFloat()

        watchBackground = Bitmap.createScaledBitmap(
            watchBackground,
            (watchBackground.width * scale).toInt(),
            (watchBackground.height * scale).toInt(),
            true
        )
    }

    override fun onDraw(canvas: Canvas, bounds: Rect) {
        drawBackground(canvas)
        drawTime(canvas)
        drawCounter(canvas)
        drawLogo(canvas)
    }

    private fun drawBackground(canvas: Canvas) = canvas.drawBitmap(watchBackground, 0f, 0f, null)

    private fun drawTime(canvas: Canvas) {
        val timeText = dateProvider.getTimeAsText()
        val timeXOffset = centerX - (paintProvider.timePaint.measureText(timeText) / 2f)

        canvas.drawText(timeText, timeXOffset, timeYOffset, paintProvider.timePaint)
    }

    private fun drawCounter(canvas: Canvas) {
        val timeLeftText = dateProvider.getTimeLeft(WEDDING_DATE).getTimeLeftDisplayFormat()
        val counterText = timeLeftText + COUNTER_UNICODE_IMAGES
        val counterXOffset = centerX - (paintProvider.counterPaint.measureText(counterText) / 2f)

        canvas.drawText(counterText, counterXOffset, countYOffset, paintProvider.counterPaint)
    }

    private fun drawLogo(canvas: Canvas) {
        val timeXOffset = centerX - (paintProvider.logoPaint.measureText(LOGO_TEXT) / 2f)
        canvas.drawText(LOGO_TEXT, timeXOffset, logoYOffset, paintProvider.logoPaint)
    }

    companion object {
        const val WEDDING_DATE = "30/07/2021 17:00:00"
        const val COUNTER_UNICODE_IMAGES = " \u279C \u2665"
        const val LOGO_TEXT = "A&A"
    }
}
