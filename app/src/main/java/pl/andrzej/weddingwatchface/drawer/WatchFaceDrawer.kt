package pl.andrzej.weddingwatchface.drawer

import android.graphics.Canvas
import android.graphics.Rect

interface WatchFaceDrawer {

    fun onCreate()

    fun onSurfaceChanged(width: Int, height: Int)

    fun onDraw(canvas: Canvas, bounds: Rect)
}
