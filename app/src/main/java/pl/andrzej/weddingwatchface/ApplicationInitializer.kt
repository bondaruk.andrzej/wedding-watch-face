package pl.andrzej.weddingwatchface

import android.content.Context
import io.reactivex.rxjava3.plugins.RxJavaPlugins
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import pl.andrzej.weddingwatchface.di.TimberKoinLogger
import pl.andrzej.weddingwatchface.di.appModule
import timber.log.Timber

object ApplicationInitializer {

    fun initComponents(context: Context) {
        initTimber()
        initKoin(context)
        initRxExceptionHandler()
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initKoin(context: Context) {
        startKoin {
            logger(TimberKoinLogger())
            androidContext(context)
            modules(appModule)
        }
    }

    private fun initRxExceptionHandler() =
        RxJavaPlugins.setErrorHandler { Timber.e(it, "Undeliverable RxJava exception occurred!") }
}
