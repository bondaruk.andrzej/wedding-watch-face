package pl.andrzej.weddingwatchface

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.Disposable
import timber.log.Timber
import java.util.concurrent.TimeUnit

class TimeUpdateTimer {

    private var disposable: Disposable? = null

    fun startTimer(timeUpdateCallback: () -> Unit) {
        stopTimer()
        disposable = Observable
            .interval(TIMER_UPDATE_INTERVAL_S, TimeUnit.SECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { timeUpdateCallback() },
                { Timber.e(it, "Error in timeout timer") }
            )
    }

    fun stopTimer() = disposable?.apply { dispose() }

    companion object {
        private const val TIMER_UPDATE_INTERVAL_S = 1L
    }
}
