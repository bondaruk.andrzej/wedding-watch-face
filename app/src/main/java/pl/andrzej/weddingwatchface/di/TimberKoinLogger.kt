package pl.andrzej.weddingwatchface.di

import org.koin.core.logger.KOIN_TAG
import org.koin.core.logger.Level
import org.koin.core.logger.Logger
import org.koin.core.logger.MESSAGE
import timber.log.Timber

class TimberKoinLogger(level: Level = Level.INFO) : Logger(level) {

    override fun log(level: Level, msg: MESSAGE) {
        if (this.level <= level) {
            logOnProperLevel(msg)
        }
    }

    private fun logOnProperLevel(message: String) {
        when (this.level) {
            Level.DEBUG -> Timber.tag(KOIN_TAG).d(message)
            Level.INFO -> Timber.tag(KOIN_TAG).i(message)
            Level.ERROR -> Timber.tag(KOIN_TAG).e(message)
            Level.NONE -> return
        }
    }
}
