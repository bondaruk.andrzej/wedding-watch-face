package pl.andrzej.weddingwatchface.di

import org.koin.dsl.module
import pl.andrzej.weddingwatchface.TimeUpdateTimer
import pl.andrzej.weddingwatchface.drawer.WatchFaceDrawer
import pl.andrzej.weddingwatchface.drawer.WatchFaceDrawerImpl
import pl.andrzej.weddingwatchface.provider.date.DateProvider
import pl.andrzej.weddingwatchface.provider.PaintProvider

val appModule = module {

    single { DateProvider() }
    single { PaintProvider(get()) }

    factory<WatchFaceDrawer> { WatchFaceDrawerImpl(get()) }
    factory { TimeUpdateTimer() }
}
