package pl.andrzej.weddingwatchface

import android.graphics.Canvas
import android.graphics.Rect
import android.os.Bundle
import android.support.wearable.watchface.CanvasWatchFaceService
import android.view.SurfaceHolder
import org.koin.android.ext.android.inject
import pl.andrzej.weddingwatchface.drawer.WatchFaceDrawer

class WeddingWatchFace : CanvasWatchFaceService() {

    override fun onCreateEngine(): Engine {
        ApplicationInitializer.initComponents(this)
        return Engine()
    }

    inner class Engine : CanvasWatchFaceService.Engine() {

        private val watchFaceDrawer: WatchFaceDrawer by inject()
        private val timeUpdateTimer: TimeUpdateTimer by inject()
        private var ambientMode = false

        override fun onCreate(holder: SurfaceHolder) {
            super.onCreate(holder)
            watchFaceDrawer.onCreate()
        }

        override fun onPropertiesChanged(properties: Bundle?) {
            super.onPropertiesChanged(properties)
        }

        override fun onTimeTick() {
            super.onTimeTick()
            invalidate()
        }

        override fun onAmbientModeChanged(inAmbientMode: Boolean) {
            super.onAmbientModeChanged(inAmbientMode)
            ambientMode = inAmbientMode
            updateTime()
        }

        private fun updateTime() {
            if (shouldTimerBeRunning()) {
                timeUpdateTimer.startTimer { invalidate() }
            } else {
                timeUpdateTimer.stopTimer()
            }
        }

        private fun shouldTimerBeRunning() = isVisible && !ambientMode

        override fun onDraw(canvas: Canvas, bounds: Rect) {
            watchFaceDrawer.onDraw(canvas, bounds)
        }

        override fun onSurfaceChanged(holder: SurfaceHolder?, format: Int, width: Int, height: Int) {
            super.onSurfaceChanged(holder, format, width, height)
            watchFaceDrawer.onSurfaceChanged(width, height)
        }

        override fun onVisibilityChanged(visible: Boolean) {
            super.onVisibilityChanged(visible)
            updateTime()
        }

        override fun onDestroy() {
            timeUpdateTimer.stopTimer()
            super.onDestroy()
        }
    }
}
