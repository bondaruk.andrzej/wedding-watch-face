package pl.andrzej.weddingwatchface.provider.date

data class TimeLeft(
    private val days: Long,
    private val hours: Long,
    private val minutes: Long,
    private val seconds: Long
) {
    fun getTimeLeftDisplayFormat(): String {
        val minutesCeiling = minutes + 1 // seconds are not displayed so minutes are increased by 1 for better reception
        return String.format(TIME_LEFT_FORMAT, days, hours, minutesCeiling)
    }

    companion object {
        private const val TIME_LEFT_FORMAT = "%dd %dh %dm"
    }
}
