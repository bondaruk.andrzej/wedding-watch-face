package pl.andrzej.weddingwatchface.provider

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import pl.andrzej.weddingwatchface.R

class PaintProvider(context: Context) {

    private val primaryFont = ResourcesCompat.getFont(context, R.font.berkshire_swash_regular)
    private val secondaryFont = ResourcesCompat.getFont(context, R.font.trajan_pro_regular)

    val timePaint = Paint().apply {
        color = Color.BLACK
        typeface = secondaryFont
        textSize = 80f
    }

    val counterPaint = Paint().apply {
        color = ContextCompat.getColor(context, R.color.time_text)
        textSize = 40f
    }

    val logoPaint = Paint().apply {
        color = ContextCompat.getColor(context, R.color.time_text)
        typeface = primaryFont
        textSize = 60f
    }
}
