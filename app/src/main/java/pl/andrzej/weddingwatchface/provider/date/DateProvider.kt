package pl.andrzej.weddingwatchface.provider.date

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoUnit
import java.util.*

class DateProvider {

    private val defaultTimeFormatter = DateTimeFormatter.ofPattern(TIME_FORMAT_DEFAULT, Locale.getDefault())
    private val timeLeftFormatter = DateTimeFormatter.ofPattern(TIME_LEFT_WEDDING_FORMATTER, Locale.getDefault())

    fun getTimeAsText(): String = defaultTimeFormatter.format(LocalDateTime.now())

    fun getTimeLeft(targetDateString: String): TimeLeft {
        val targetDate = LocalDateTime.parse(targetDateString, timeLeftFormatter)
        var currentTime = LocalDateTime.now()

        val daysLeft = currentTime.until(targetDate, ChronoUnit.DAYS)
        currentTime = currentTime.plusDays(daysLeft)

        val hoursLeft = currentTime.until(targetDate, ChronoUnit.HOURS)
        currentTime = currentTime.plusHours(hoursLeft)

        val minutesLeft = currentTime.until(targetDate, ChronoUnit.MINUTES)
        currentTime = currentTime.plusMinutes(minutesLeft)

        val secondsLeft = currentTime.until(targetDate, ChronoUnit.SECONDS)
        return TimeLeft(daysLeft, hoursLeft, minutesLeft, secondsLeft)
    }

    companion object {
        const val TIME_LEFT_WEDDING_FORMATTER = "dd/MM/yyyy HH:mm:ss"
        private const val TIME_FORMAT_DEFAULT = "HH:mm"
    }
}
