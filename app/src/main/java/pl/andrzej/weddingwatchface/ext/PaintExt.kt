package pl.andrzej.weddingwatchface.ext

import android.graphics.Paint

fun Paint.getFontHeight(): Float {
    return fontMetrics.descent - fontMetrics.ascent
}
